"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
var env_1 = require("./env");
var fuse_box_typechecker_1 = require("fuse-box-typechecker");
var path = require("path");
var fuse_box_1 = require("fuse-box");
// Configure the environment
var env = env_1.getClientEnvironment();
var isProduction = process.env.NODE_ENV === 'production';
// Configure directories
var root = path.resolve(__dirname, '../');
var directory = {
    node_modules: path.join(root, 'node_modules'),
    build: path.join(root, 'build'),
    cache: path.join(root, '.fusebox'),
    public: path.join(root, 'public'),
    src: path.join(root, 'src'),
    assets: path.join(root, 'src', 'assets'),
    styles: path.join(root, 'src', 'styles'),
};
// Configure type checker
var tsConfigPath = path.join(directory.src, 'tsconfig.json');
var tsLintPath = path.join(directory.src, 'tslint.json');
var typechecker = fuse_box_typechecker_1.TypeChecker({
    name: 'app',
    tsConfig: tsConfigPath,
    basePath: directory.src,
    tsLint: tsLintPath,
    yellowOnLint: true,
    shortenFilenames: true
});
var runSyncTypeChecker = function () {
    console.log("\u001B[36m%s\u001B[0m", "prod app bundled - running type check");
    typechecker.runSync();
};
if (!isProduction) {
    // Create thread
    typechecker.startTreadAndWait();
}
var runThreadTypeChecker = function () {
    console.log("\u001B[36m%s\u001B[0m", "dev app bundled - running type check");
    // Use thread, tell it to typecheck and print result
    typechecker.useThreadAndTypecheck();
};
var stylePlugins = [
    fuse_box_1.SassPlugin(),
    fuse_box_1.CSSPlugin({
        outFile: function (file) { return directory.build + "/" + file; }
    })
];
var styleDevPlugins = [
    fuse_box_1.SassPlugin(),
    fuse_box_1.PostCSSPlugin([require("postcss-scss")]),
    fuse_box_1.CSSPlugin()
];
// Generic fuse configuration
function getConfig() {
    var plugins = [
        fuse_box_1.EnvPlugin(env.raw),
        fuse_box_1.WebIndexPlugin({
            target: "index.html",
            template: directory.public + "/index.html",
            path: '/'
        }),
        fuse_box_1.CopyPlugin({
            files: ['*.png', '*.jpg', '*.svg', '*.html, *.ico, *.json'],
            dest: 'public'
        })
    ];
    if (isProduction) {
        plugins.push(fuse_box_1.QuantumPlugin({
            bakeApiIntoBundle: 'app',
            removeExportsInterop: true,
            processPolyfill: true,
            polyfills: ['Promise'],
            target: 'browser',
            replaceTypeOf: true,
            treeshake: true,
            uglify: { es6: true }
        }));
    }
    return fuse_box_1.FuseBox.init({
        homeDir: directory.src,
        sourceMaps: !isProduction,
        debug: !isProduction,
        log: {
            showBundledFiles: false,
            clearTerminalOnBundle: true // Clear the terminal window every time we bundle
        },
        cache: !isProduction,
        target: 'browser',
        output: directory.build + "/$name.js",
        hash: isProduction,
        allowSyntheticDefaultImports: true,
        useTypescriptCompiler: true,
        plugins: plugins,
        alias: {
            app: "~",
            assets: '~/assets',
            components: '~/components',
            containers: '~/containers',
            enums: '~/enums',
            index: '~/index',
            logger: '~/logger',
            models: '~/models',
            pages: '~/pages',
            router: '~/router',
            store: '~/store',
            theme: '~/theme',
            styles: '~/styles',
            types: '~/types',
            utils: '~/utils'
        }
    });
}
// Clean the build directory
fuse_box_1.Sparky.task('clean', [], function () { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, fuse_box_1.Sparky.src("" + directory.cache)
                    .clean("" + directory.cache)
                    .exec()];
            case 1:
                _a.sent();
                return [4 /*yield*/, fuse_box_1.Sparky.src("" + directory.build)
                        .clean("" + directory.build)
                        .exec()];
            case 2: return [2 /*return*/, _a.sent()];
        }
    });
}); });
// Copy static content / assets
fuse_box_1.Sparky.task('copy', [], function () { return __awaiter(_this, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, new Promise(function (resolve, reject) {
                child_process_1.exec("cp -r " + directory.assets + " " + directory.build, function (err, stdout, stderr) {
                    resolve();
                });
                child_process_1.exec("cp -r " + directory.public + "/* " + directory.build, function (err, stdout, stderr) {
                    resolve();
                });
            })];
    });
}); });
fuse_box_1.Sparky.task('production', [], function () { return __awaiter(_this, void 0, void 0, function () {
    var fuseInstance;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                fuseInstance = getConfig();
                fuseInstance
                    .bundle('app')
                    .tsConfig(tsConfigPath)
                    .target('browser')
                    .plugin(stylePlugins.slice())
                    .completed(function () {
                    runSyncTypeChecker();
                })
                    .splitConfig({ dest: '/chunks/' })
                    .instructions("> index.tsx");
                return [4 /*yield*/, fuseInstance.run()];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); });
fuse_box_1.Sparky.task('development', [], function () { return __awaiter(_this, void 0, void 0, function () {
    var fuseInstance;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                fuseInstance = getConfig();
                fuseInstance.dev({ fallback: 'index.html' });
                fuseInstance
                    .bundle('app')
                    .tsConfig(tsConfigPath)
                    .target('browser')
                    .plugin(styleDevPlugins.slice())
                    .completed(function () {
                    runThreadTypeChecker();
                })
                    .watch()
                    .instructions("> index.tsx")
                    .hmr();
                return [4 /*yield*/, fuseInstance.run()];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); });
fuse_box_1.Sparky.task('build', ['clean', 'copy', 'production'], function () {
    return;
});
fuse_box_1.Sparky.task('default', ['clean', 'copy', 'development'], function () {
    return;
});
