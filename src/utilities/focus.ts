import * as React from 'react';

export function handleMouseUpByBlurring({
                                            currentTarget,
                                        }: React.MouseEvent<HTMLAnchorElement | HTMLButtonElement>) {
    currentTarget.blur();
}