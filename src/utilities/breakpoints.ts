import { noop } from '@shopify/javascript-utilities/other';

const Breakpoints = {
    navigationBarCollapsed: '769px',
    stackedContent: '1043px',
    ribbonContainerCollapsed: '1456px',
};

const noWindowMatches: MediaQueryList = {
    media: '',
    addListener: noop,
    removeListener: noop,
    matches: false,
    onchange: noop,
    addEventListener: noop,
    removeEventListener: noop,
    dispatchEvent: (_: Event) => true,
};

export function navigationBarCollapsed() {
    return typeof window === 'undefined'
        ? noWindowMatches
        : window.matchMedia(`(max-width: ${Breakpoints.navigationBarCollapsed})`);
}

export function ribbonContainerCollapsed() {
    return typeof window === 'undefined'
        ? noWindowMatches
        : window.matchMedia(`(max-width: ${Breakpoints.ribbonContainerCollapsed})`);
}

export function stackedContent() {
    return typeof window === 'undefined'
        ? noWindowMatches
        : window.matchMedia(`(max-width: ${Breakpoints.stackedContent})`);
}