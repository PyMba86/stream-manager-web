import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Card from "../../components/Card/Card";
import DescriptionList from "../../components/DescriptionList";
import Heading from "../../components/Heading";
import Layout from "../../components/Layout/Layout";
import Page from "../../components/Page/Page";
import { ResourceList } from "../../components/ResourceList/ResourceList";
import TextContainer from "../../components/TextContainer";
import TextStyle from "../../components/TextStyle";
import { Control } from "../../models/Control";
import ConnectionsStore from "../../store/ConnectionsStore";
import DevicesStore from "../../store/DevicesStore";


interface State {
    control: Control;
}


@observer
export class ControlPage extends React.Component {

    @inject('devicesStore')
    public devicesStore: DevicesStore;

    @inject('connectionsStore')
    public connectionsStore: ConnectionsStore;

    @inject('routerStore')
    public routerStore: RouterStore;

    public state: State = {
        control: {name: "", key: "", actions: [], variables: []},
    };

    public componentDidMount() {
        const {params} = this.routerStore.routerState;

        // Получаем информацию по устройству
        this.devicesStore.detail(params.key).then((control) => {
            this.setState({control});

            // Подписываемся на устройство
            this.connectionsStore.subscribe(control.key).then(value => {
                console.log("subscribe success");
                // Получаем значения переменных
                control.variables.map(
                    (value) => {
                        this.devicesStore.call(control.key, value).then(result => {
                                this.devicesStore.setVariable(value, result);
                            }
                        );
                    });
            });

        });
    }

    public componentWillUnmount() {
        const {control} = this.state;
        this.connectionsStore.unsubscribe(control.key).then(value => {
            console.log("unsuscribe success");
        });
    }

    public disabledDevice = () => {
        const {control} = this.state;
        this.devicesStore.disconnect(control.key).then(value => {
            console.log("disconnect success");
        });
    };


    public render() {
        const {control} = this.state;

        const pageMarkup = (
            <Page
                fullWidth={true}
                title={control.name}
                secondaryActions={[{content: 'Отключить', onAction: this.disabledDevice}]}
            >
                <Layout>
                    <Layout.Section oneHalf={true}>
                        <TextContainer>
                            <Heading>Действия</Heading>
                            <Card>
                                <ResourceList
                                    resourceName={{singular: 'device-action', plural: 'device-actions'}}
                                    items={control.actions.map(
                                        (value, index) => {
                                            return {
                                                id: index,
                                                action: value
                                            };
                                        })}
                                    renderItem={(item: { id: string, action: string }) => {
                                        return (
                                            <ResourceList.Item
                                                id={String(item.id)}
                                                onClick={(id) => {
                                                    this.devicesStore.call(control.key, item.action);
                                                }}
                                                context={{
                                                    selectMode: false,
                                                    resourceName: {singular: 'action', plural: 'actions'}
                                                }}
                                            >
                                                <h3>
                                                    <TextStyle variation="strong">{item.action}</TextStyle>
                                                </h3>
                                            </ResourceList.Item>
                                        );
                                    }}
                                />
                            </Card>
                        </TextContainer>
                    </Layout.Section>
                    <Layout.Section oneHalf={true}>
                        <TextContainer>
                            <Heading>Переменные</Heading>
                            <DescriptionList
                                items={control.variables.map(
                                    (value, index) => {
                                        return {
                                            id: index,
                                            term: value,
                                            description: String(this.devicesStore.getVariables.get(value))
                                        };
                                    })}
                            />
                        </TextContainer>
                    </Layout.Section>
                </Layout>
            </Page>
        );

        return (
            <div>
                {pageMarkup}
            </div>
        );
    }
}
