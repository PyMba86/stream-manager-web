import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Avatar from "../../components/Avatar/Avatar";
import Card from "../../components/Card/Card";
import Layout from "../../components/Layout/Layout";
import Page from "../../components/Page/Page";
import { ResourceList } from "../../components/ResourceList/ResourceList";
import TextStyle from "../../components/TextStyle/TextStyle";
import SubscriptionsStore from "../../store/SubscriptionsStore";


interface Item {
    id: number;
    key: string;
}

@observer
export class OverviewPage extends React.Component {

    @inject('subscriptionsStore')
    public subscriptionsStore: SubscriptionsStore;

    @inject('routerStore')
    public routerStore: RouterStore;

    public componentDidMount() {
        // Получаем список устройств авторизованного пользователя
        this.subscriptionsStore.devices().then((value) => {
            console.log("load users devices list");
        });
    }

    public render() {

        const pageMarkup = (
            <Page
                fullWidth={true}
                title='Обзор'
            >
                <Layout>
                    <Layout.Section>
                        <Card>
                            <ResourceList
                                resourceName={{singular: 'device', plural: 'devices'}}
                                items={this.subscriptionsStore.getDevices.map(
                                    (value, index) => {
                                        return {
                                            id: index,
                                            key: value
                                        };
                                    })}
                                renderItem={(item: Item) => {
                                    const media = <Avatar initials={item.key.charAt(0)} size="medium" name={name}/>;

                                    return (
                                        <ResourceList.Item
                                            id={String(item.id)}
                                            onClick={(id) => {
                                                this.routerStore.goTo('control', {key: item.key});
                                            }}
                                            media={media}
                                            context={{
                                                selectMode: false,
                                                resourceName: {singular: 'device', plural: 'devices'}
                                            }}
                                        >
                                            <h3>
                                                <TextStyle variation="strong">{item.key}
                                                </TextStyle>
                                            </h3>
                                        </ResourceList.Item>
                                    );
                                }}
                            />
                        </Card>
                    </Layout.Section>
                </Layout>
            </Page>
        );

        return (
            <div>
                {pageMarkup}
            </div>
        );
    }
}
