import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Avatar from "../../components/Avatar/Avatar";
import Card from "../../components/Card/Card";
import Layout from "../../components/Layout/Layout";
import Modal from "../../components/Modal/Modal";
import Page from "../../components/Page/Page";
import { ResourceList } from "../../components/ResourceList/ResourceList";
import TextField from "../../components/TextField/TextField";
import TextStyle from "../../components/TextStyle/TextStyle";
import { Device } from "../../models/Device";
import DevicesStore from "../../store/DevicesStore";


interface Item extends Device {
    id: number;
}

interface State {
    activeAddModal: boolean;
    addNameDevice: string;
}


@observer
export class DevicesPage extends React.Component {

    @inject('devicesStore')
    public devicesStore: DevicesStore;

    @inject('routerStore')
    public routerStore: RouterStore;

    public state: State = {
        activeAddModal: false,
        addNameDevice: "",
    };

    public handleDelete = (key: string) => {
        // Удаляем пользователя
        this.devicesStore.delete(key).then(value =>
            console.log("delete device " + key)
        );
    };

    public componentDidMount() {
        // Получаем список пользователей
        this.devicesStore.list().then((value) => {
            console.log("load devices list");
        });
    }

    public handleAddClick = () => {
        // Добавление нового пользователя
        const {addNameDevice} = this.state;
        if (addNameDevice.length > 3) {
            this.devicesStore.add(addNameDevice).then(value => {
                console.log("add device");
                this.toggleAddModal();
            });
        } else {
            console.error("name size > 3 required new device");
        }
    };

    public toggleAddModal = () => {
        this.setState({activeAddModal: !this.state.activeAddModal, addNameDevice: ""});
    };


    public handleChange = (field: string) => {
        return (value: any) => this.setState({[field]: value});
    };

    public render() {
        const {activeAddModal, addNameDevice} = this.state;

        const modalAddUserMarkup = (
            <Modal
                open={activeAddModal}
                large={true}
                onClose={this.toggleAddModal}
                title="Устройство"
                primaryAction={{
                    content: 'Добавить',
                    onAction: this.handleAddClick,
                }}
            >
                <Modal.Section>
                    <TextField
                        label="Название"
                        value={addNameDevice}
                        onChange={this.handleChange('addNameDevice')}
                    />
                </Modal.Section>
            </Modal>
        );

        const pageMarkup = (
            <Page
                fullWidth={true}
                title='Устройства'
                primaryAction={{content: 'Добавить', onAction: this.toggleAddModal}}
            >
                <Layout>
                    <Layout.Section>
                        <Card>
                            <ResourceList
                                resourceName={{singular: 'device', plural: 'devices'}}
                                items={this.devicesStore.getDevices.map(
                                    (value, index) => {
                                        return {
                                            id: index,
                                            ...value
                                        };
                                    })}
                                renderItem={(item: Item) => {
                                    const media = <Avatar initials={item.name.charAt(0)} size="medium" name={name}/>;
                                    const shortcutActions =
                                        [
                                            {
                                                content: 'Удалить',
                                                onAction: () => {
                                                    if (item.key) {
                                                        this.handleDelete(item.key);
                                                    }
                                                }
                                            },
                                        ];

                                    const keyMarkup = item.key ? (
                                        <TextStyle variation="subdued"> ({item.key})</TextStyle>
                                    ) : null;

                                    return (
                                        <ResourceList.Item
                                            id={String(item.id)}
                                            onClick={(id) => {
                                                this.routerStore.goTo('control', {key: item.key});
                                            }}
                                            media={media}
                                            shortcutActions={shortcutActions}
                                            persistActions={true}
                                            context={{
                                                selectMode: false,
                                                resourceName: {singular: 'device', plural: 'devices'}
                                            }}
                                        >
                                            <h3>
                                                <TextStyle variation="strong">{item.name}
                                                </TextStyle>
                                                {keyMarkup}
                                            </h3>
                                        </ResourceList.Item>
                                    );
                                }}
                            />
                        </Card>
                    </Layout.Section>
                </Layout>
            </Page>
        );

        return (
            <div>
                {modalAddUserMarkup}
                {pageMarkup}
            </div>
        );
    }
}
