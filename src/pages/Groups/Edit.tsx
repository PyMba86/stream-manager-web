import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Avatar from "../../components/Avatar/Avatar";
import Card from "../../components/Card/Card";
import Heading from "../../components/Heading/Heading";
import Layout from "../../components/Layout/Layout";
import Modal from "../../components/Modal/Modal";
import Page from "../../components/Page/Page";
import { ResourceList } from "../../components/ResourceList/ResourceList";
import TextContainer from "../../components/TextContainer/TextContainer";
import TextStyle from "../../components/TextStyle/TextStyle";
import { Device } from "../../models/Device";
import ConnectionsStore from "../../store/ConnectionsStore";
import DevicesStore from "../../store/DevicesStore";
import GroupsStore from "../../store/GroupsStore";
import SubscriptionsStore from "../../store/SubscriptionsStore";
import UsersStore from "../../store/UsersStore";


interface ItemUser {
    id: number;
    login: string;
}

interface ItemDevice extends Device {
    id: number;
}

interface State {
    activeAddUserModal: boolean;
    activeAddDeviceModal: boolean;
}


@observer
export class GroupsEditPage extends React.Component {

    @inject('routerStore')
    public routerStore: RouterStore;

    @inject('subscriptionsStore')
    public subscriptionsStore: SubscriptionsStore;

    @inject('groupsStore')
    public groupsStore: GroupsStore;

    @inject('usersStore')
    public usersStore: UsersStore;

    @inject('connectionsStore')
    public connectionsStore: ConnectionsStore;

    @inject('devicesStore')
    public devicesStore: DevicesStore;

    public state: State = {
        activeAddUserModal: false,
        activeAddDeviceModal: false
    };

    public handleDeleteGroup = (key: string) => {
        this.groupsStore.delete(key).then(value => {
                console.log("delete group " + key);
                this.routerStore.goTo('groups');
            }
        );
    };

    public componentDidMount() {
        const {params} = this.routerStore.routerState;
        this.loadUsers(params.key);
        this.loadDevices(params.key);
        this.loadAllUsers();
        this.loadAllDevices();
    }

    public handleUserDelete = (key: string, login: string) => {
        this.subscriptionsStore.delete(key, login).then(value => {
                console.log("delete user group " + key);
            }
        );
    };

    public handleUserAddClick = (key: string, login: string) => {
        this.subscriptionsStore.add(key, login).then(value => {
                console.log("add user group " + key);
                this.toggleAddUserModal();
            }
        );
    };

    public handleDeviceDelete = (deviceKey: string, groupKey: string) => {
        this.connectionsStore.delete(deviceKey, groupKey).then(value => {
                console.log("delete device group " + deviceKey);
            }
        );
    };

    public handleDeviceAddClick = (deviceKey: string, groupKey: string) => {
        this.connectionsStore.add(deviceKey, groupKey).then(value => {
                console.log("add device group " + deviceKey);
                this.toggleAddDeviceModal();
            }
        );
    };


    public loadUsers(key: string) {
        // Получаем список пользователей группы
        this.subscriptionsStore.list(key).then((users) => {
            console.log("load users group " + key);
        });
    }

    public loadDevices(key: string) {
        // Получаем список устройств группы
        this.connectionsStore.list(key).then((users) => {
            console.log("load devices group " + key);
        });
    }

    public loadAllUsers(): void {
        this.usersStore.list().then((users) => {
            console.log("load users");
        });
    }

    public loadAllDevices(): void {
        this.devicesStore.list().then((users) => {
            console.log("load devices");
        });
    }

    public toggleAddUserModal = () => {
        this.setState({activeAddUserModal: !this.state.activeAddUserModal});
    };

    public toggleAddDeviceModal = () => {
        this.setState({activeAddDeviceModal: !this.state.activeAddDeviceModal});
    };

    public render() {
        const {params} = this.routerStore.routerState;
        const {activeAddUserModal, activeAddDeviceModal} = this.state;

        const modalAddUserMarkup = (
            <Modal
                open={activeAddUserModal}
                onClose={this.toggleAddUserModal}
                title="Пользователи"
            >
                <Modal.Section>
                    <ResourceList
                        resourceName={{singular: 'group-user', plural: 'group-users'}}
                        items={this.usersStore.getUsers.map(
                            (value, index) => {
                                return {
                                    id: index,
                                    login: value.login
                                };
                            }).filter(user => this.subscriptionsStore.getSubscriptions.indexOf(user.login) < 0)}
                        renderItem={(item: ItemUser) => {
                            const media = <Avatar initials={item.login.charAt(0)} size="medium" name={name}/>;
                            return (
                                <ResourceList.Item
                                    id={String(item.id)}
                                    onClick={(id) => this.handleUserAddClick(params.key, item.login)}
                                    media={media}
                                    persistActions={true}
                                    context={{
                                        selectMode: false,
                                        resourceName: {singular: 'user', plural: 'users'}
                                    }}
                                >
                                    <h3>
                                        <TextStyle variation="strong">{item.login}</TextStyle>
                                    </h3>
                                </ResourceList.Item>
                            );
                        }}
                    />
                </Modal.Section>
            </Modal>
        );

        const modalAddDeviceMarkup = (
            <Modal
                open={activeAddDeviceModal}
                onClose={this.toggleAddDeviceModal}
                title="Устройства"
            >
                <Modal.Section>
                    <ResourceList
                        resourceName={{singular: 'group-device', plural: 'group-devices'}}
                        items={this.devicesStore.getDevices.map(
                            (value, index) => {
                                return {
                                    id: index,
                                    ...value
                                };
                            }).filter(device => this.connectionsStore.getConnections.indexOf(device.key) < 0)}
                        renderItem={(item: ItemDevice) => {
                            const media = <Avatar initials={item.name.charAt(0)} size="medium" name={name}/>;
                            return (
                                <ResourceList.Item
                                    id={String(item.id)}
                                    onClick={(id) => this.handleDeviceAddClick(item.key, params.key)}
                                    media={media}
                                    persistActions={true}
                                    context={{
                                        selectMode: false,
                                        resourceName: {singular: 'device', plural: 'devices'}
                                    }}
                                >
                                    <h3>
                                        <TextStyle variation="strong">{item.name}</TextStyle>
                                    </h3>
                                </ResourceList.Item>
                            );
                        }}
                    />
                </Modal.Section>
            </Modal>
        );

        const pageMarkup = (
            <Page
                fullWidth={true}
                title={params.key}
                breadcrumbs={[{content: 'Группы', onAction: () => this.routerStore.goTo('groups')}]}
                primaryAction={{content: 'Удалить', onAction: () => this.handleDeleteGroup(params.key)}}
                secondaryActions={[
                    {content: 'Добавить пользователя', onAction: this.toggleAddUserModal},
                    {content: 'Добавить устройство', onAction: this.toggleAddDeviceModal}
                ]}
            >
                <Layout>
                    <Layout.Section oneHalf={true}>
                        <TextContainer>
                            <Heading>Пользователи</Heading>
                            <Card>
                                <ResourceList
                                    resourceName={{singular: 'group-user', plural: 'group-users'}}
                                    items={this.subscriptionsStore.getSubscriptions.map(
                                        (value, index) => {
                                            return {
                                                id: index,
                                                login: value
                                            };
                                        })}
                                    renderItem={(item: ItemUser) => {
                                        const media = <Avatar initials={item.login.charAt(0)} size="medium"
                                                              name={name}/>;
                                        const shortcutActions =
                                            [
                                                {
                                                    content: 'Удалить',
                                                    onAction: () => this.handleUserDelete(params.key, item.login)
                                                },
                                            ];


                                        return (
                                            <ResourceList.Item
                                                id={String(item.id)}
                                                onClick={(id) => {
                                                    console.log(id);
                                                }}
                                                media={media}
                                                shortcutActions={shortcutActions}
                                                persistActions={true}
                                                context={{
                                                    selectMode: false,
                                                    resourceName: {singular: 'user', plural: 'users'}
                                                }}
                                            >
                                                <h3>
                                                    <TextStyle variation="strong">{item.login}</TextStyle>
                                                </h3>
                                            </ResourceList.Item>
                                        );
                                    }}
                                />
                            </Card>
                        </TextContainer>
                    </Layout.Section>
                    <Layout.Section oneHalf={true}>
                        <TextContainer>
                            <Heading>Устройства</Heading>
                            <Card>
                                <ResourceList
                                    resourceName={{singular: 'group-user', plural: 'group-users'}}
                                    items={this.devicesStore.getDevices.map(
                                        (value, index) => {
                                            return {
                                                id: index,
                                                ...value
                                            };
                                        }).filter(device => this.connectionsStore.getConnections.includes(device.key))}
                                    renderItem={(item: ItemDevice) => {
                                        const media = <Avatar initials={item.name.charAt(0)} size="medium"
                                                              name={name}/>;
                                        const shortcutActions =
                                            [
                                                {
                                                    content: 'Удалить',
                                                    onAction: () => this.handleDeviceDelete( item.key, params.key)
                                                },
                                            ];


                                        return (
                                            <ResourceList.Item
                                                id={String(item.id)}
                                                onClick={(id) => {
                                                    console.log(id);
                                                }}
                                                media={media}
                                                shortcutActions={shortcutActions}
                                                persistActions={true}
                                                context={{
                                                    selectMode: false,
                                                    resourceName: {singular: 'device', plural: 'devices'}
                                                }}
                                            >
                                                <h3>
                                                    <TextStyle variation="strong">{item.name}</TextStyle>
                                                </h3>
                                            </ResourceList.Item>
                                        );
                                    }}
                                />
                            </Card>
                        </TextContainer>
                    </Layout.Section>
                </Layout>
            </Page>
        );

        return (
            <div>
                {modalAddUserMarkup}
                {modalAddDeviceMarkup}
                {pageMarkup}
            </div>
        );
    }
}
