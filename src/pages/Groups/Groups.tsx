import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Avatar from "../../components/Avatar/Avatar";
import Card from "../../components/Card/Card";
import Layout from "../../components/Layout/Layout";
import Modal from "../../components/Modal/Modal";
import Page from "../../components/Page/Page";
import { ResourceList } from "../../components/ResourceList/ResourceList";
import TextField from "../../components/TextField/TextField";
import TextStyle from "../../components/TextStyle/TextStyle";
import { Group } from "../../models/Group";
import GroupsStore from "../../store/GroupsStore";


interface Item extends Group {
    id: number;
}

interface State {
    activeAddModal: boolean;
    addNameGroup: string;
    addKeyGroup: string;
}


@observer
export class GroupsPage extends React.Component {

    @inject('groupsStore')
    public groupsStore: GroupsStore;

    @inject('routerStore')
    public routerStore: RouterStore;

    public state: State = {
        activeAddModal: false,
        addKeyGroup: "",
        addNameGroup: ""
    };

    public handleDelete = (key: string) => {
        // Удаляем пользователя
        this.groupsStore.delete(key).then(value =>
            console.log("delete group " + key)
        );
    };

    public componentDidMount() {
        // Получаем список пользователей
        this.groupsStore.list().then((value) => {
            console.log("load groups list");
        });
    }

    public handleAddClick = () => {
        // Добавление нового пользователя
        const {addNameGroup, addKeyGroup} = this.state;
        if (addKeyGroup.length > 3 && addNameGroup.length > 3) {
            this.groupsStore.add({key: addKeyGroup, name: addNameGroup}).then(value => {
                console.log("add group");
                this.toggleAddModal();
            });
        } else {
            console.error("key and name size > 3 required new group");
        }
    };

    public toggleAddModal = () => {
        this.setState({activeAddModal: !this.state.activeAddModal, addNameGroup: "", addKeyGroup: ""});
    };


    public handleChange = (field: string) => {
        return (value: any) => this.setState({[field]: value});
    };

    public render() {
        const {activeAddModal, addNameGroup, addKeyGroup} = this.state;

        const modalAddUserMarkup = (
            <Modal
                open={activeAddModal}
                large={true}
                onClose={this.toggleAddModal}
                title="Группа"
                primaryAction={{
                    content: 'Добавить',
                    onAction: this.handleAddClick,
                }}
            >
                <Modal.Section>
                    <TextField
                        label="Ключ"
                        value={addKeyGroup}
                        onChange={this.handleChange('addKeyGroup')}
                    />
                    <TextField
                        label="Название"
                        value={addNameGroup}
                        onChange={this.handleChange('addNameGroup')}
                    />
                </Modal.Section>
            </Modal>
        );

        const pageMarkup = (
            <Page
                fullWidth={true}
                title='Группы'
                primaryAction={{content: 'Добавить', onAction: this.toggleAddModal}}
            >
                <Layout>
                    <Layout.Section>
                        <Card>
                            <ResourceList
                                resourceName={{singular: 'group', plural: 'groups'}}
                                items={this.groupsStore.getGroups.map(
                                    (value, index) => {
                                        return {
                                            id: index,
                                            ...value
                                        };
                                    })}
                                renderItem={(item: Item) => {
                                    const media = <Avatar initials={item.name.charAt(0)} size="medium" name={name}/>;
                                    const shortcutActions =
                                        [
                                            {
                                                content: 'Удалить',
                                                onAction: () => this.handleDelete(item.key)
                                            },
                                        ];

                                    const keyMarkup = item.key ? (
                                        <TextStyle variation="subdued"> ({item.key})</TextStyle>
                                    ) : null;

                                    return (
                                        <ResourceList.Item
                                            id={String(item.id)}
                                            onClick={(key) => {
                                                this.routerStore.goTo('groups-edit', {key: item.key});
                                            }}
                                            media={media}
                                            shortcutActions={shortcutActions}
                                            persistActions={true}
                                            context={{
                                                selectMode: false,
                                                resourceName: {singular: 'user', plural: 'users'}
                                            }}
                                        >
                                            <h3>
                                                <TextStyle variation="strong">{item.name}
                                                </TextStyle>
                                                {keyMarkup}
                                            </h3>
                                        </ResourceList.Item>
                                    );
                                }}
                            />
                        </Card>
                    </Layout.Section>
                </Layout>
            </Page>
        );

        return (
            <div>
                {modalAddUserMarkup}
                {pageMarkup}
            </div>
        );
    }
}
