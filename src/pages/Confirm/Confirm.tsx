import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Card from "../../components/Card/Card";
import Form from "../../components/Form/Form";
import FormLayout from "../../components/FormLayout/FormLayout";
import TextField from "../../components/TextField/TextField";
import ManagerInfoStore from "../../store/ManagerInfoStore";
import styles from './Confirm.scss';

@observer
export class ConfirmPage extends React.Component {

    @inject('routerStore')
    public routerStore: RouterStore;

    @inject('managerInfoStore')
    public managerInfoStore: ManagerInfoStore;

    public state = {
        key: '',
        readonly: false
    };

    public handleChange = (field: any) => {
        return (value: any) => this.setState({[field]: value});
    };

    public handleSubmit = () => {
        const {key} = this.state;
        this.managerInfoStore.setKey(key);
        this.routerStore.goTo('login');
    };

    public componentDidMount() {
        this.managerInfoStore.info().then((value) => {
            this.setState({readonly: value.length > 0, key: value});
        }).catch(reason => {
            this.setState({readonly: false});
        });
    }

    public render() {
        const {key, readonly} = this.state;
        return (
            <div className={styles.Confirm}>
                <div className={styles.ConfirmCard}>
                    <Card sectioned={true}
                          title="Менеджер"
                          primaryFooterAction={{
                              content: 'Продолжить',
                              onAction: this.handleSubmit,
                              disabled: !(key.length > 0)
                          }}
                    >
                        <Form onSubmit={this.handleSubmit}>
                            <FormLayout>
                                <TextField
                                    value={key}
                                    onChange={this.handleChange('key')}
                                    label="Ключ"
                                    disabled={readonly}
                                    type="text"
                                    helpText={<span>Ключ доступа к менеджеру</span>}
                                />
                            </FormLayout>
                        </Form>
                    </Card>
                </div>
            </div>
        );
    }
}
