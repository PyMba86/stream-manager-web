import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Avatar from "../../components/Avatar/Avatar";
import Card from "../../components/Card/Card";
import Layout from "../../components/Layout/Layout";
import Modal from "../../components/Modal/Modal";
import Page from "../../components/Page/Page";
import { ResourceList } from "../../components/ResourceList/ResourceList";
import TextField from "../../components/TextField/TextField";
import TextStyle from "../../components/TextStyle/TextStyle";
import { User } from "../../models/User";
import UsersStore from "../../store/UsersStore";


interface Item extends User {
    id: number;
}

interface State {
    activeAddModal: boolean;
    addUserLogin: string;
    addUserPassword: string;
}


@observer
export class UsersPage extends React.Component {

    @inject('usersStore')
    public usersStore: UsersStore;

    public state: State = {
        activeAddModal: false,
        addUserLogin: "",
        addUserPassword: ""
    };

    public handleDelete = (login: string) => {
        // Удаляем пользователя
        this.usersStore.delete(login).then(value =>
            console.log("delete user " + login)
        );
    };

    public componentDidMount() {
        // Получаем список пользователей
        this.usersStore.list().then((value) => {
            console.log("load users list");
        });
    }

    public handleAddClick = () => {
        // Добавление нового пользователя
        const {addUserLogin, addUserPassword} = this.state;
        if (addUserLogin.length >= 6 && addUserPassword.length >= 6) {
            this.usersStore.add({login: addUserLogin, password: addUserPassword}).then(value => {
                console.log("add user");
                this.toggleAddModal();
            });
        } else {
            console.error("login and password size >= 6 required new user");
        }
    };

    public toggleAddModal = () => {
        this.setState({activeAddModal: !this.state.activeAddModal, addUserLogin: "", addUserPassword: ""});
    };


    public handleChange = (field: string) => {
        return (value: any) => this.setState({[field]: value});
    };

    public render() {
        const {activeAddModal, addUserLogin, addUserPassword} = this.state;

        const modalAddUserMarkup = (
            <Modal
                open={activeAddModal}
                large={true}
                onClose={this.toggleAddModal}
                title="Пользователь"
                primaryAction={{
                    content: 'Добавить',
                    onAction: this.handleAddClick,
                }}
            >
                <Modal.Section>
                    <TextField
                        label="Логин"
                        value={addUserLogin}
                        onChange={this.handleChange('addUserLogin')}
                    />
                    <TextField
                        label="Пароль"
                        value={addUserPassword}
                        onChange={this.handleChange('addUserPassword')}
                    />
                </Modal.Section>
            </Modal>
        );

        const pageMarkup = (
            <Page
                fullWidth={true}
                title='Пользователи'
                primaryAction={{content: 'Добавить', onAction: this.toggleAddModal}}
            >
                <Layout>
                    <Layout.Section>
                        <Card>
                            <ResourceList
                                resourceName={{singular: 'user', plural: 'users'}}
                                items={this.usersStore.getUsers.map(
                                    (value, index) => {
                                        return {
                                            id: index,
                                            ...value
                                        };
                                    })}
                                renderItem={(item: Item) => {
                                    const media = <Avatar initials={item.login.charAt(0)} size="medium" name={name}/>;
                                    const shortcutActions =
                                        [
                                            {
                                                content: 'Удалить',
                                                onAction: () => this.handleDelete(item.login)
                                            },
                                        ];

                                    const adminMarkup =  item.admin ? (
                                        <TextStyle variation="subdued"> (администратор)</TextStyle>
                                    ) : null;

                                    return (
                                        <ResourceList.Item
                                            id={String(item.id)}
                                            onClick={(id) => {
                                                console.log(id);
                                            }}
                                            media={media}
                                            shortcutActions={shortcutActions}
                                            persistActions={true}
                                            context={{
                                                selectMode: false,
                                                resourceName: {singular: 'user', plural: 'users'}
                                            }}
                                        >
                                            <h3>
                                                <TextStyle variation="strong">{item.login}
                                                </TextStyle>
                                                {adminMarkup}
                                            </h3>
                                        </ResourceList.Item>
                                    );
                                }}
                            />
                        </Card>
                    </Layout.Section>
                </Layout>
            </Page>
        );

        return (
            <div>
                {modalAddUserMarkup}
                {pageMarkup}
            </div>
        );
    }
}
