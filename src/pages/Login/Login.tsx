import { RouterStore } from "mobx-state-router";
import * as React from 'react';
import { inject, observer } from 'ts-mobx-react';
import Card from "../../components/Card/Card";
import Form from "../../components/Form/Form";
import FormLayout from "../../components/FormLayout/FormLayout";
import TextField from "../../components/TextField/TextField";
import ManagerInfoStore from "../../store/ManagerInfoStore";
import UsersStore from "../../store/UsersStore";
import styles from './Login.scss';

@observer
export class LoginPage extends React.Component {

    @inject('routerStore')
    public routerStore: RouterStore;

    @inject('usersStore')
    public usersStore: UsersStore;

    @inject('managerInfoStore')
    public managerInfoStore: ManagerInfoStore;

    public state = {
        login: '',
        password: ''
    };

    public handleChange = (field: any) => {
        return (value: any) => this.setState({[field]: value});
    };

    public handleSubmit = () => {
        this.usersStore.login({login: this.state.login, password: this.state.password})
            .then(status => {
                this.routerStore.goTo("overview");
            })
        ;
    };

    public handleManagerSubmit = () => {
        if (this.managerInfoStore.exit()) {
            this.routerStore.goTo("confirm");
        }
    };

    public render() {
        const {password, login} = this.state;
        return (
            <div className={styles.Login}>
                <div className={styles.LoginCard}>
                    <Card sectioned={true}
                          title="stream-manager-web"
                          primaryFooterAction={{content: 'Войти', onAction: this.handleSubmit}}
                          secondaryFooterAction={{content: 'Менеджер', onAction: this.handleManagerSubmit}}
                    >
                        <Form onSubmit={this.handleSubmit}>
                            <FormLayout>
                                <TextField
                                    value={login}
                                    onChange={this.handleChange('login')}
                                    label="Пользователь"
                                    type="text"
                                    helpText={<span>Логин пользователя</span>}
                                />
                                <TextField
                                    value={password}
                                    onChange={this.handleChange('password')}
                                    label="Пароль"
                                    type="password"
                                    helpText={<span>Пароль пользователя</span>}
                                />
                            </FormLayout>
                        </Form>
                    </Card>
                </div>
            </div>
        );
    }
}
