import { RouterStore } from 'mobx-state-router';
import React from 'react';
import { inject, observer } from 'ts-mobx-react';
import AppProvider from "../../components/AppProvider/AppProvider";
import Frame from "../../components/Frame";
import { Loading } from "../../components/Loading/Loading";
import Navigation from "../../components/Navigation/Navigation";
import TopBar from "../../components/TopBar";
import UsersStore from "../../store/UsersStore";


@observer
export class DashboardLayout extends React.Component {

    @inject('routerStore')
    public routerStore: RouterStore;

    @inject('usersStore')
    public usersStore: UsersStore;

    public state = {
        isLoading: false,
        isDirty: false,
        searchActive: false,
        userMenuOpen: false,
        logoMenuOpen: false,
        showMobileNavigation: false,
        modalActive: false,
        supportSubject: '',
        supportMessage: '',
    };

    public render() {
        const {
            isLoading,
            searchActive,
            userMenuOpen,
            logoMenuOpen,
            showMobileNavigation
        } = this.state;

        const {
            children
        } = this.props;

        const userMenuActions = [
            {
                items: [{
                    content: 'Выйти', onAction: () => {
                        this.usersStore.logout().then(resolve => {
                            this.routerStore.goTo("login");
                        });
                    }
                }],
            },
        ];

        const logoMenuActions = [
            {
                items: [
                    {content: 'home'},
                    {content: 'test'},
                    {content: 'work'}
                ],
            },
        ];

        const userMenuMarkup = (
            <TopBar.UserMenu
                actions={userMenuActions}
                name={this.usersStore.authUser.login}
                detail={this.usersStore.authUser.admin ? "Администратор" : "Пользователь"}
                initials={this.usersStore.authUser.login.charAt(0).toUpperCase()}
                open={userMenuOpen}
                onToggle={this.toggleState('userMenuOpen')}
            />
        );

        const logoMenuMarkup = (
            <TopBar.UserMenu
                actions={logoMenuActions}
                name="home"
                detail="stream-cloud"
                initials="H"
                open={logoMenuOpen}
                onToggle={this.toggleState('logoMenuOpen')}
            />
        );

        const topBarMarkup = (
            <TopBar
                showNavigationToggle={true}
                userMenu={userMenuMarkup}
                logoMenu={logoMenuMarkup}
                searchResultsVisible={searchActive}
                onSearchResultsDismiss={this.handleSearchResultsDismiss}
                onNavigationToggle={this.toggleState('showMobileNavigation')}
            />
        );

        const navigationMarkup = this.usersStore.authUser.admin ? (
            <Navigation location="/">
                <Navigation.Section
                    items={[
                        {
                            url: '/dashboard/overview',
                            label: 'Обзор',
                            icon: 'home',
                            onClick: () => this.routerStore.goTo('overview'),
                        },
                        {
                            url: '/dashboard/groups',
                            label: 'Группы',
                            icon: 'orders',
                            onClick: () => this.routerStore.goTo('groups')
                        },
                        {
                            url: '/dashboard/devices',
                            label: 'Устройства',
                            icon: 'products',
                            onClick: () => this.routerStore.goTo('devices')
                        },
                        {
                            url: '/dashboard/users',
                            label: 'Пользователи',
                            icon: 'alert',
                            onClick: () => this.routerStore.goTo('users')
                        },
                        {
                            url: '/path/to/place',
                            label: 'История',
                            icon: 'view',
                        },
                        {
                            url: '/path/to/place',
                            label: 'Сценарии',
                            icon: 'notes',
                        },
                        {
                            url: '/dashboard/settings',
                            label: 'Настройки',
                            icon: 'delete',
                            onClick: () => this.routerStore.goTo('settings')
                        },
                    ]}
                />
                <Navigation.Section
                    separator={false}
                    title="Комнаты"
                    items={[
                        {
                            label: 'Зал',
                            icon: 'products',
                            onClick: this.toggleState('isLoading'),
                        },
                        {
                            label: 'Детская',
                            icon: 'products',
                            onClick: this.toggleState('isLoading'),
                        },
                        {
                            label: 'Кухня',
                            icon: 'products',
                            onClick: this.toggleState('isLoading'),
                        },
                        {
                            label: 'Спальня',
                            icon: 'products',
                            onClick: this.toggleState('isLoading'),
                        },
                    ]}
                    action={{
                        icon: 'circlePlus',
                        accessibilityLabel: 'Добавить комнату',
                        onClick: this.toggleState('modalActive'),
                    }}
                />
            </Navigation>
        ) : null;

        const loadingMarkup = isLoading ? <Loading/> : null;

        const globalRibbonMarkup = (
            <div style={{position: "fixed", padding: "1em"}}>
                История
            </div>
        );

        return (
            <AppProvider>
                <Frame
                    topBar={topBarMarkup}
                    navigation={navigationMarkup}
                    showMobileNavigation={showMobileNavigation}
                    onNavigationDismiss={this.toggleState('showMobileNavigation')}
                    globalRibbon={globalRibbonMarkup}
                >
                    {loadingMarkup}
                    {children}
                </Frame>
            </AppProvider>
        );
    }

    public toggleState = (key: string) => {
        return () => {
            this.setState((prevState) => ({[key]: !prevState[key]}));
        };
    };

    public handleSearchFieldChange = (value: string) => {
        this.setState({searchText: value});
        if (value.length > 0) {
            this.setState({searchActive: true});
        } else {
            this.setState({searchActive: false});
        }
    };


    public handleSearchResultsDismiss = () => {
        this.setState(() => {
            return {
                searchActive: false,
                searchText: '',
            };
        });
    };

}
