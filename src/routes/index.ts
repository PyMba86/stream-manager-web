import { RouterState, RouterStore } from "mobx-state-router";

const checkContainsUserKey = (fromState: RouterState, toState: RouterState, routerStore: RouterStore) => {
    const {
        rootStore: {usersStore, managerInfoStore}
    } = routerStore;
    if (managerInfoStore.hasKey()) {
        if (usersStore.checkAuth()) {
            return Promise.resolve();
        } else {
            return Promise.reject(new RouterState('login'));
        }
    } else {
        return Promise.reject(new RouterState('confirm'));
    }

};

const checkContainsUserCookieKey = (fromState: RouterState, toState: RouterState, routerStore: RouterStore) => {
    const {
        rootStore: {usersStore, managerInfoStore}
    } = routerStore;
    if (managerInfoStore.hasKey()) {
        if (!usersStore.checkAuth()) {
            return Promise.resolve();
        } else {
            return Promise.reject(new RouterState('overview'));
        }
    } else {
        return Promise.reject(new RouterState('confirm'));
    }
};

const checkAdmin = (fromState: RouterState, toState: RouterState, routerStore: RouterStore) => {
    const {
        rootStore: {usersStore, managerInfoStore}
    } = routerStore;
    if (managerInfoStore.hasKey()) {
        if (usersStore.checkAuth() && usersStore.checkAdmin()) {
            return Promise.resolve();
        } else {
            return Promise.reject(new RouterState('notFound'));
        }
    } else {
        return Promise.reject(new RouterState('confirm'));
    }
};

export const routes = [
    {name: 'login', pattern: '/login', beforeEnter: checkContainsUserCookieKey},
    {name: 'confirm', pattern: '/confirm'},
    {name: 'overview', pattern: '/', beforeEnter: checkContainsUserKey},
    {name: 'control', pattern: '/control/:key', beforeEnter: checkContainsUserKey},
    {name: 'overview', pattern: '/overview', beforeEnter: checkContainsUserKey},
    {name: 'groups', pattern: '/groups', beforeEnter: checkAdmin},
    {name: 'groups-edit', pattern: '/groups/:key', beforeEnter: checkAdmin},
    {name: 'users', pattern: '/users', beforeEnter: checkAdmin},
    {name: 'devices', pattern: '/devices', beforeEnter: checkAdmin},
    {name: 'notFound', pattern: '/not-found'}
];