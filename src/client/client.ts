import { TSMap } from "typescript-map";
import { Handler as RpcHandler } from "./rpc/handler";
import { Socket as SocketClient } from "./websocket/socket";

export class Client {

    public methodHandlers: Array<(method: string, params?: any) => any> = [];

    private rpc: RpcHandler;
    private status: boolean;
    public metadata: TSMap<string, string>;

    constructor(socket: SocketClient) {
        this.rpc = new RpcHandler();
        this.rpc.sender = (message => socket.send(message));
        this.metadata = new TSMap();

        socket.onmessage = (event: any) => this.rpc.receive(event.data).catch(console.error);
        socket.onopen = (event: any) => this.status = true;

        this.rpc.methodHandler = (method, params) => {
            return this.callMethodHandlers(method, params);
        };
    }

    public call(method: string, params?: any, metadata?: any): Promise<any> {
        return new Promise<any>((resolve) => {
            setTimeout(
                (): void => {
                    if (this.status) {
                        const meta: any = this.metadata.toJSON();
                        resolve(this.rpc.call(method, params, Object.assign(meta, metadata)));
                    } else {
                        resolve(this.call(method, params));
                    }
                }, 3);

        });
    }

    protected callMethodHandlers(method: string, params: any): void {
        for (const handler of this.methodHandlers) {
            try {
                return handler(method, params);
            } catch (e) {
                throw e;
            }
        }
    }
}