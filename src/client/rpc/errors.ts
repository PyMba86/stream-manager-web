import { ResponseError } from "./messages";


export enum ErrorCode {
    ParseError = -32700,
    InvalidRequest = -32600,
    MethodNotFound = -32601,
    InvalidParams = -32602,
    InternalError = -32603,
}

const ServerErrorSince = -32000;
const ServerErrorUntil = -32099;


export class JsonRpcError extends Error implements ResponseError {

    public code: number;
    public data: any;

    constructor(code: number = ErrorCode.InternalError, message?: string, data?: any) {
        super(message || makeDefaultErrorMessage(code));
        this.name = "JsonRpcError";
        this.code = code;
        this.data = data;
    }


    public toJSON(): ResponseError {
        const json: ResponseError = {
            code: Number(this.code),
            message: String(this.message),
        };
        if (this.data !== undefined) {
            json.data = this.data;
        }
        return json;
    }
}

function makeDefaultErrorMessage(code: number): string {
    switch (code) {
        case ErrorCode.ParseError:
            return "Parse error";
        case ErrorCode.InvalidRequest:
            return "Invalid Request";
        case ErrorCode.MethodNotFound:
            return "Method not found";
        case ErrorCode.InvalidParams:
            return "Invalid params";
        case ErrorCode.InternalError:
            return "Internal error";
    }
    if (code >= ServerErrorSince && code <= ServerErrorUntil) {
        return "Server error";
    }
    return "Unknown Error";
}