

export interface NotifyMessage {
    jsonrpc: string;
    method: string;
    params?: any;
    metadata?: any;
}