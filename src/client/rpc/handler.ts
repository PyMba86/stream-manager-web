import { ErrorCode, JsonRpcError } from "./errors";
import { RequestMessage, ResponseMessage } from "./messages";
import { NotifyMessage } from "./messages/notify";

export const VERSION = "2.0";
const MAX_INT32 = 2147483647;

export class Handler {

    public sender: (message: string) => any | Promise<any>;

    public methodHandler: (method: string, params: any, metadata?: any) => any | Promise<any>;

    public timeout: number = 60000;

    protected idCounter: number = 0;

    protected callbackMap: Map<number | string, { resolve: (result: any) => void, reject: (error: JsonRpcError) => void }> = new Map();


    public async call(method: string, params?: any, metadata?: any, id?: number | string): Promise<any> {
        const self = this;
        return new Promise<any>((resolve, reject) => {
            const req = this.createRequest(method, params, id, metadata);
            this.callbackMap.set(req.id, {resolve, reject});
            try {
                const result = this.sender(JSON.stringify(req));
                if (isPromise(result)) {
                    result.catch(removeIdAndReject);
                }
                if (this.timeout > 0) {
                    setTimeout(() => {
                        if (this.callbackMap.has(req.id)) {
                            const e = new Error(`RPC response timeouted. (${JSON.stringify(req)})`);
                            e.name = "TimeoutError";
                            removeIdAndReject(e);
                        }
                    }, this.timeout);
                }
            } catch (e) {
                removeIdAndReject(e);
            }

            function removeIdAndReject(e: any): void {
                self.callbackMap.delete(req.id);
                reject(e);
            }
        });
    }


    public async receive(message: string): Promise<void> {
        await this.doMetodOrCallback(this.parse(message));
    }


    public async doMetodOrCallback(json: NotifyMessage | NotifyMessage[] | ResponseMessage | ResponseMessage[])
        : Promise<NotifyMessage | NotifyMessage[] | void> {
        if (!Array.isArray(json)) {
            if (this.isResponse(json)) {
                this.doCallback(json as ResponseMessage);
            } else {
                return this.doMethod(json as NotifyMessage);
            }
        } else {
            const responses: any[] = [];
            for (const j of json) {
                if (this.isResponse(j)) {
                    this.doCallback(j as ResponseMessage);
                } else if (this.isNotify(j)) {
                    await this.doMethod(j as NotifyMessage);
                }
            }
            if (responses.length > 0) {
                return responses;
            }
        }
    }

    protected async doMethod(notify: NotifyMessage): Promise<NotifyMessage | void> {
        const result = this.methodHandler(notify.method, notify.params, notify.metadata);
        if (isPromise(result)) {
            await result;
        }
    }

    protected doCallback(response: ResponseMessage): void {
        const cb = this.callbackMap.get(response.id);
        if (!cb) {
            return;
        }
        this.callbackMap.delete(response.id);
        if (response.error) {
            cb.reject(new JsonRpcError(response.error.code, response.error.message, response.error.data));
        } else {
            cb.resolve(response.result);
        }
    }

    public createRequest(method: string, params?: any, id?: string | number, metadata?: any): RequestMessage {
        if (id === null || id === undefined) {
            id = this.generateId();
        } else if (typeof (id) !== "number") {
            id = String(id);
        }
        return {jsonrpc: VERSION, method, params, id, metadata};
    }

    public createResponse(id: string | number, result: any, error?: any): ResponseMessage {
        if (typeof (id) !== "number") {
            id = String(id);
        }
        const res: ResponseMessage = {jsonrpc: VERSION, id};
        if (error) {
            res.error = error;
        } else {
            res.result = result || null;
        }
        return res;
    }

    public parse(message: string): RequestMessage | RequestMessage[] | ResponseMessage | ResponseMessage[] {
        let json: any;
        try {
            json = JSON.parse(message);
        } catch (e) {
            throw new JsonRpcError(ErrorCode.ParseError);
        }
        if (!(json instanceof Object)) {
            throw new JsonRpcError(ErrorCode.InvalidRequest);
        }
        if (Array.isArray(json) && json.length === 0) {
            throw new JsonRpcError(ErrorCode.InvalidRequest);
        }
        return json;
    }

    public isResponse(json: any): boolean {
        return json.result !== undefined || json.error !== undefined;
    }

    public isNotify(json: any): boolean {
        return json.method !== undefined || json.id === undefined;
    }

    private generateId(): number {
        if (this.idCounter >= MAX_INT32) {
            this.idCounter = 0;
        }
        return ++this.idCounter;
    }
}

function isPromise(obj: any): boolean {
    return obj instanceof Promise || (obj && typeof obj.then === 'function');
}