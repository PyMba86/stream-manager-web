// import { createBrowserHistory } from 'history';
import { HistoryAdapter, RouterView } from 'mobx-state-router';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { observer, Provider } from 'ts-mobx-react';
import { RootStore } from "./store/RootStore";
import * as serviceWorker from './utils/serviceWorker';

import { WithLayout } from "./components/WithLayout/WithLayout";
import { DashboardLayout } from "./layouts/DashboardLayout/DashboardLayout";
import { ConfirmPage } from "./pages/Confirm/Confirm";
import { ControlPage } from "./pages/Control/Control";
import { DevicesPage } from "./pages/Devices/Devices";
import { GroupsEditPage } from "./pages/Groups/Edit";
import { GroupsPage } from "./pages/Groups/Groups";
import { LoginPage } from "./pages/Login/Login";
import { NotFoundPage } from "./pages/NotFound/NotFound";
import { OverviewPage } from "./pages/Overview/Overview";
import { UsersPage } from "./pages/Users/Users";

import { Client as RpcClient } from "./client/client";
import { Socket as SocketClient } from "./client/websocket/socket";


 import createHashHistory from "history/createHashHistory";
import './styles/global.scss';


// Create websocket
let urlClient: string = "ws://shome.ooo:8091";

if (process.env.NODE_ENV === 'production') {
    // 8091 - Универсальный порт для менеджера и платформы
    urlClient = "ws://shome.ooo:8091";
}

const socket = new SocketClient(urlClient);

// Create rpc client
const client = new RpcClient(socket);

// Create the rootStore
const rootStore = new RootStore(client);

// Observe history changes
 const historyAdapter = new HistoryAdapter(rootStore.routerStore, createHashHistory());
// const historyAdapter = new HistoryAdapter(rootStore.routerStore, createBrowserHistory());
historyAdapter.observeRouterStateChanges();

@observer
export class Main extends React.Component {


    public render() {

        const viewMap = {
            'login': <LoginPage/>,
            'confirm': <ConfirmPage/>,
            'notFound': <NotFoundPage/>,
            'overview': WithLayout(OverviewPage, DashboardLayout),
            'control': WithLayout(ControlPage, DashboardLayout),
            'groups': WithLayout(GroupsPage, DashboardLayout),
            'devices': WithLayout(DevicesPage, DashboardLayout),
            'users': WithLayout(UsersPage, DashboardLayout),
            'groups-edit': WithLayout(GroupsEditPage, DashboardLayout)
        };

        return (
            <Provider {...rootStore} rootStore={rootStore} routerStore={rootStore.routerStore}>
                <RouterView routerStore={rootStore.routerStore} viewMap={viewMap}/>
            </Provider>
        );
    }
}

ReactDOM.render(<Main/>, document.getElementById('root') as HTMLElement);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();