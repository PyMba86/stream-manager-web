import { action } from 'mobx';
import { Client as RpcClient } from "../client/client";
import { ManagerInfoService } from "../services/ManagerInfoService";

export const MANAGER_KEY: string = "manager-key";

export default class ManagerInfoStore {

    public service: ManagerInfoService;

    private client: RpcClient;

    constructor(client: RpcClient) {
        this.service = new ManagerInfoService(client);
        this.client = client;

        const key: string | null = localStorage.getItem(MANAGER_KEY);
        if (key) {
            this.client.metadata.set(MANAGER_KEY, key);
        }
    }

    public hasKey(): boolean {
        return this.client.metadata.has(MANAGER_KEY);
    }

    public setKey(key: string) {
        console.log(key);
        this.client.metadata.set(MANAGER_KEY, key);
        localStorage.setItem(MANAGER_KEY, key);
    }

    @action
    public async info(): Promise<string> {
        return this.service.info()
            .then(result => {
                this.setKey(result);
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public exit(): boolean {
        this.client.metadata.delete(MANAGER_KEY);
        localStorage.setItem(MANAGER_KEY, '');
        return true;
    }

}