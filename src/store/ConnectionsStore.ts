import { action, computed, observable } from 'mobx';
import { Client as RpcClient } from "../client/client";
import { ConnectionsService } from "../services/ConnectionsService";


export default class ConnectionsStore {

    public service: ConnectionsService;

    @observable
    public devices: string[];

    constructor(client: RpcClient) {
        this.service = new ConnectionsService(client);
        this.devices = [];
    }

    @action
    public async list(groupKey: string): Promise<string[]> {
        return this.service.list(groupKey)
            .then(value => {
                this.devices = value;
                return value;
            })
            .catch(reason => {
                this.devices = [];
                throw reason;
            });
    }

    @action
    public async delete(deviceKey: string, groupKey: string): Promise<boolean> {
        return this.service.delete(deviceKey, groupKey)
            .then(value => {
                if (value) {
                    this.devices = this.devices.filter(device => device !== deviceKey);
                }
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async add(deviceKey: string, groupKey: string): Promise<boolean> {
        return this.service.add(deviceKey, groupKey)
            .then(result => {
                if (result) {
                    this.devices.push(deviceKey);
                }
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public subscribe(deviceKey: string): Promise<boolean> {
        return this.service.subscribe(deviceKey)
            .then(result => {
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public unsubscribe(deviceKey: string): Promise<boolean> {
        return this.service.unsubscribe(deviceKey)
            .then(result => {
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @computed
    get getConnections() {
        return this.devices;
    }


}