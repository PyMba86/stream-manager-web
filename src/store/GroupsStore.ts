import { action, computed, observable } from 'mobx';
import { Client as RpcClient } from "../client/client";
import { Group } from "../models/Group";
import { GroupsService } from "../services/GroupsService";


export default class GroupsStore {

    public service: GroupsService;

    @observable
    public groups: Group[];

    constructor(client: RpcClient) {
        this.service = new GroupsService(client);
        this.groups = [];
    }

    @action
    public async list(): Promise<Group[]> {
        return this.service.list()
            .then(value => {
                this.groups = value;
                return value;
            })
            .catch(reason => {
                this.groups = [];
                throw reason;
            });
    }

    @action
    public async delete(key: string): Promise<boolean> {
        return this.service.delete(key)
            .then(value => {
                if (value) {
                    this.groups = this.groups.filter(group => group.key !== key);
                }
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async add(group: Group): Promise<boolean> {
        return this.service.add(group)
            .then(result => {
                if (result) {
                    this.groups.push(group);
                }
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @computed
    get getGroups() {
        return this.groups;
    }


}