import { action, computed, observable } from 'mobx';
import { Control } from "models/Control";
import { Client as RpcClient } from "../client/client";
import { Device } from "../models/Device";
import { DevicesService } from "../services/DevicesService";


export default class GroupsStore {

    public service: DevicesService;

    @observable
    public devices: Device[];

    @observable
    public control: Control;

    private client: RpcClient;

    @observable
    public variables: Map<string, string>;

    constructor(client: RpcClient) {
        this.service = new DevicesService(client);
        this.devices = [];
        this.variables = new Map();
        this.client = client;
    }

    @action
    public async list(): Promise<Device[]> {
        return this.service.list()
            .then(value => {
                this.devices = value;
                return value;
            })
            .catch(reason => {
                this.devices = [];
                throw reason;
            });
    }

    @action
    public async delete(key: string): Promise<boolean> {
        return this.service.delete(key)
            .then(value => {
                if (value) {
                    this.devices = this.devices.filter(device => device.key !== key);
                }
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async add(name: string): Promise<string> {
        return this.service.add(name)
            .then(result => {
                if (result) {
                    this.devices.push({key: result, name});
                }
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async call(deviceKey: string, method: string): Promise<string> {
        return this.service.control(deviceKey, method)
            .then(result => {
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async setVariable(variable: string, value: string) {
        this.variables.set(variable, value);
    }


    @action
    public async detail(key: string): Promise<Control> {
        return this.service.detail(key)
            .then(result => {
                this.control = result;

                result.variables.map(name => {
                    this.variables.set(name, "Не определен");
                });

                // Служит для обратного вызова - со стороны платформы
                // Доделать на подписку
                this.client.methodHandlers.push((method, params) => {
                    if (result.variables.includes(method)) {
                        this.setVariable(method, params);
                    }
                });

                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public disconnect(deviceKey: string): Promise<void> {
        return this.service.disconnect(deviceKey)
            .then(result => {
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @computed
    get getDevices() {
        return this.devices;
    }

    @computed
    get getControl() {
        return this.control;
    }

    @computed
    get getVariables() {
        return this.variables;
    }


}