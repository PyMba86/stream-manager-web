import { action, computed, observable } from 'mobx';
import { Client as RpcClient } from "../client/client";
import { AuthUser } from "../models/AuthUser";
import { User } from "../models/User";
import { UsersService } from "../services/UsersService";

export const USER_KEY: string = "user-key";
export const USER_LOGIN: string = "user-login";
export const USER_ADMIN: string = "user-admin";

export default class UsersStore {

    public service: UsersService;

    @observable
    public users: User[];
    public authUser: AuthUser;
    private client: RpcClient;

    constructor(client: RpcClient) {
        this.service = new UsersService(client);
        this.users = [];
        this.client = client;

        const key: string | null = localStorage.getItem(USER_KEY);
        if (key) {
            this.client.metadata.set(USER_KEY, key);
        }
    }

    public checkAuth(): boolean {
        const cookieUserKey = localStorage.getItem(USER_KEY);
        const cookieUserLogin = localStorage.getItem(USER_LOGIN);
        const cookieUserAdmin = localStorage.getItem(USER_ADMIN) === "1";

        if (cookieUserKey && cookieUserLogin && this.client.metadata.has(USER_KEY)) {
            this.authUser = {key: cookieUserKey, admin: cookieUserAdmin, login: cookieUserLogin};
            return true;
        }
        return false;
    }

    public checkAdmin(): boolean {
        return localStorage.getItem(USER_ADMIN) === "1";
    }

    @action
    public async list(): Promise<User[]> {
        return this.service.list()
            .then(value => {
                this.users = value;
                return value;
            })
            .catch(reason => {
                this.users = [];
                throw reason;
            });
    }

    @action
    public async delete(login: string): Promise<boolean> {
        return this.service.delete(login)
            .then(value => {
                if (value) {
                    this.users = this.users.filter(user => user.login !== login);
                }
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async add(user: User): Promise<boolean> {
        return this.service.add(user)
            .then(managerInfo => {
                if (managerInfo) {
                    this.users.push(user);
                }
                return managerInfo;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async login(user: User): Promise<boolean> {
        return this.service.login(user)
            .then(authUser => {
                this.authUser = authUser;
                this.client.metadata.set(USER_KEY, authUser.key);
                localStorage.setItem(USER_KEY, authUser.key);
                localStorage.setItem(USER_ADMIN, authUser.admin ? "1" : "0");
                localStorage.setItem(USER_LOGIN, authUser.login);
                return true;
            })
            .catch(reason => {
                this.client.metadata.delete(USER_KEY);
                localStorage.removeItem(USER_KEY);
                localStorage.removeItem(USER_ADMIN);
                localStorage.removeItem(USER_LOGIN);
                throw reason;
                return false;
            });
    }

    @action
    public async logout(): Promise<void> {
        return new Promise<void>(resolve => {
            this.authUser = {login: '', key: '', admin: false};
            this.client.metadata.delete(USER_KEY);
            localStorage.removeItem(USER_KEY);
            localStorage.removeItem(USER_LOGIN);
            localStorage.removeItem(USER_ADMIN);
            resolve();
        });
    }

    @computed
    get getUsers() {
        return this.users;
    }


}