import { RouterState, RouterStore } from 'mobx-state-router';

import { routes } from "../routes";

import { Client as RpcClient } from "../client/client";
import ConnectionsStore from "./ConnectionsStore";
import DevicesStore from "./DevicesStore";
import GroupsStore from "./GroupsStore";
import ManagerInfoStore from "./ManagerInfoStore";
import SubscriptionsStore from "./SubscriptionsStore";
import UsersStore from "./UsersStore";


const notFound = new RouterState('notFound');

export class RootStore {
    public routerStore: RouterStore;
    public usersStore: UsersStore;
    public groupsStore: GroupsStore;
    public subscriptionsStore: SubscriptionsStore;
    public devicesStore: DevicesStore;
    public connectionsStore: ConnectionsStore;
    public managerInfoStore: ManagerInfoStore;


    constructor(client: RpcClient) {
        this.routerStore = new RouterStore(this, routes, notFound);
        this.usersStore = new UsersStore(client);
        this.groupsStore = new GroupsStore(client);
        this.subscriptionsStore = new SubscriptionsStore(client);
        this.devicesStore = new DevicesStore(client);
        this.connectionsStore = new ConnectionsStore(client);
        this.managerInfoStore = new ManagerInfoStore(client);
    }
}