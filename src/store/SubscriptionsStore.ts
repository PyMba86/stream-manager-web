import { action, computed, observable } from 'mobx';
import { Client as RpcClient } from "../client/client";
import { SubscriptionsService } from "../services/SubscriptionsService";


export default class SubscriptionsStore {

    public service: SubscriptionsService;

    @observable
    public subscriptions: string[];

    @observable
    public subDevices: string[];

    constructor(client: RpcClient) {
        this.service = new SubscriptionsService(client);
        this.subscriptions = [];
        this.subDevices = [];
    }

    @action
    public async list(key: string): Promise<string[]> {
        return this.service.list(key)
            .then(value => {
                this.subscriptions = value;
                return value;
            })
            .catch(reason => {
                this.subscriptions = [];
                throw reason;
            });
    }

    @action
    public async delete(key: string, login: string): Promise<boolean> {
        return this.service.delete(key, login)
            .then(value => {
                if (value) {
                    this.subscriptions = this.subscriptions.filter(subscription => subscription !== login);
                }
                return value;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public async add(key: string, login: string): Promise<boolean> {
        return this.service.add(key, login)
            .then(result => {
                if (result) {
                    this.subscriptions.push(login);
                }
                return result;
            })
            .catch(reason => {
                throw reason;
            });
    }

    @action
    public devices(): Promise<string[]> {
        return this.service.devices()
            .then(result => {
                console.log(result);
                this.subDevices = result;
                return result;
            })
            .catch(reason => {
                this.subDevices = [];
                throw reason;
            });
    }

    @computed
    get getSubscriptions() {
        return this.subscriptions;
    }

    @computed
    get getDevices() {
        return this.subDevices;
    }


}