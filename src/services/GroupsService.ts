import { Client as RpcClient } from "../client/client";
import { Group } from "../models/Group";


export class GroupsService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public list(): Promise<Group[]> {
        return this.client.call("groups.list");
    }

    public delete(key: string): Promise<boolean> {
        return this.client.call("groups.delete", {"key": key});
    }

    public add(group: Group): Promise<boolean> {
        return this.client.call("groups.add", {"key": group.key, "name": group.name});
    }
}