import { Client as RpcClient } from "../client/client";


export class ManagerInfoService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public info(): Promise<string> {
        return this.client.call("manager.info");
    }

}