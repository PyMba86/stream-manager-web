import { Client as RpcClient } from "../client/client";


export class ConnectionsService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public list(groupKey: string): Promise<string[]> {
        return this.client.call("connections.list", {"group-key": groupKey});
    }

    public delete(deviceKey: string, groupKey: string): Promise<boolean> {
        return this.client.call("connections.delete", {"device-key": deviceKey, "group-key": groupKey});
    }

    public add(deviceKey: string, groupKey: string): Promise<boolean> {
        return this.client.call("connections.add", {"device-key": deviceKey, "group-key": groupKey});
    }

    public subscribe(deviceKey: string): Promise<boolean> {
        return this.client.call("connections.subscribe", {"device-key": deviceKey});
    }

    public unsubscribe(deviceKey: string): Promise<boolean> {
        return this.client.call("connections.unsubscribe", {"device-key": deviceKey});
    }
}