import { Client as RpcClient } from "../client/client";
import { Control } from "../models/Control";
import { Device } from "../models/Device";


export class DevicesService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public list(): Promise<Device[]> {
        return this.client.call("devices.list");
    }

    public delete(key: string): Promise<boolean> {
        return this.client.call("devices.delete", {"key": key});
    }

    public add(name: string): Promise<string> {
        return this.client.call("devices.add", {"name": name});
    }

    public detail(key: string): Promise<Control> {
        return this.client.call("devices.detail", {"key": key});
    }

    public control(key: string, method: string): Promise<string> {
        return this.client.call("devices.control", {"action": method}, {"device-key": key});
    }

    public disconnect(key: string): Promise<void> {
        return this.client.call("devices.disconnect", {"key": key});
    }
}