import { Client as RpcClient } from "../client/client";


export class SubscriptionsService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public list(key: string): Promise<string[]> {
        return this.client.call("subscriptions.list", {"group-key": key});
    }

    public delete(key: string, login: string): Promise<boolean> {
        return this.client.call("subscriptions.delete", {"group-key": key, "user-login": login});
    }

    public add(key: string, login: string): Promise<boolean> {
        return this.client.call("subscriptions.add", {"group-key": key, "user-login": login});
    }

    public devices(): Promise<string[]> {
        return this.client.call("subscriptions.devices");
    }
}