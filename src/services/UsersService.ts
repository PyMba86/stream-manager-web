import { Client as RpcClient } from "../client/client";
import { AuthUser } from "../models/AuthUser";
import { User } from "../models/User";

export class UsersService {

    public client: RpcClient;

    constructor(client: RpcClient) {
        this.client = client;
    }

    public list(): Promise<User[]> {
        return this.client.call("users.list");
    }

    public delete(login: string): Promise<boolean> {
        return this.client.call("users.delete", {"login": login});
    }

    public add(user: User): Promise<boolean> {
        return this.client.call("users.add", {"login": user.login, "password": user.password});
    }

    public login(user: User): Promise<AuthUser> {
        return this.client.call("users.login", {"login": user.login, "password": user.password});
    }
}