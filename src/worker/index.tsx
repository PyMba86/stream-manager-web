const CACHE_NAME: string = 'stream-manager-web-v1';
const urlsToCache = [
    '/',
    '/bundle.css',
    '/app.js',
    '/index.html',
    '/favicon.ico',
    '/manifest.json'
];


self.addEventListener('install', async (event: any): Promise<void> => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(cache => {
                // Open a cache and cache our files
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('activate', async (event: ExtendableEvent): Promise<void> => {

    // Белый список
    const cacheWhitelist = [CACHE_NAME];

    event.waitUntil(
        caches.keys().then(cacheNames => Promise.all(
            cacheNames.map(cacheName => {
                if (cacheWhitelist.indexOf(cacheName) === -1) {
                    return caches.delete(cacheName);
                }
                return true;
            })
        ))
    );
});

self.addEventListener('fetch', (event: FetchEvent): void => {
    event.respondWith(
        caches.match(event.request).then(response => response || fetch(event.request))
    );
});

export default self;