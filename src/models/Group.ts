export interface Group {
    key: string;
    name: string;
}