export interface User {
    login: string;
    password?: string;
    admin?: boolean;
}