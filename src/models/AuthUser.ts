export interface AuthUser {
    key: string;
    admin: boolean;
    login: string;
}