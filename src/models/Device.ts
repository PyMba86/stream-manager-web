export interface Device {
    key: string;
    name: string;
}