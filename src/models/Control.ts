export interface Control {
    key: string;
    name: string;
    actions: string[];
    variables: string[];
}