import {
    addEventListener,
    removeEventListener,
} from '@shopify/javascript-utilities/events';
import * as React from 'react';

export interface BaseEventProps {
    event: string;
    capture?: boolean;
    handler(event: Event): void;
}

export interface Props extends BaseEventProps {
    passive?: boolean;
}

// see https://github.com/oliviertassinari/react-event-listener/
export default class EventListener extends React.PureComponent<Props, never> {
    public componentDidMount() {
        this.attachListener();
    }

    public componentDidUpdate({passive, ...detachProps}: Props) {
        this.detachListener(detachProps);
        this.attachListener();
    }

    public componentWillUnmount() {
        this.detachListener();
    }

    public render() {
        return null;
    }

    private attachListener() {
        const {event, handler, capture, passive} = this.props;
        addEventListener(window, event, handler, {capture, passive});
    }

    private detachListener(prevProps?: BaseEventProps) {
        const {event, handler, capture} = prevProps || this.props;
        removeEventListener(window, event, handler, capture);
    }
}