import * as PropTypes from 'prop-types';

export interface FrameManager {
    //  showToast(toast: { id: string } & ToastProps): void;

    //  hideToast(toast: { id: string }): void;

    // setContextualSaveBar(props: ContextualSaveBarProps): void;

    // removeContextualSaveBar(): void;

    startLoading(): void;

    stopLoading(): void;
}

export interface FrameContext {
    frame: FrameManager;
}

export const frameContextTypes = {
    frame: PropTypes.object,
};
