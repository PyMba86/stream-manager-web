import * as React from 'react';

import { ContextualSaveBarProps as Props } from '../../../ContextualSaveBar';


import styles from './ContextualSaveBar.scss';

export type CombinedProps = Props;

export interface State {
    discardConfirmationModalVisible: boolean;
}

class ContextualSaveBar extends React.PureComponent<CombinedProps, State> {
    public state: State = {
        discardConfirmationModalVisible: false,
    };

    public render() {

        const {
            message,
            discardAction,
        } = this.props;

        const discardConfirmationModalMarkup = discardAction &&
            discardAction.onAction;

        return (
            <React.Fragment>
                <div className={styles.ContextualSaveBar}>

                    <div className={styles.Contents}>
                        <h2 className={styles.Message}>{message}</h2>
                    </div>
                </div>
                {discardConfirmationModalMarkup}
            </React.Fragment>
        );
    }

}

export default ContextualSaveBar;