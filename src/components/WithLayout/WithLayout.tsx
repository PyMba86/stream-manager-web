import * as React from "react";

export function WithLayout(Component: React.ComponentClass, Layout: React.ComponentClass) {
    return <Layout><Component/></Layout>;
}