import { ReactComponent } from '@shopify/react-utilities/types';
import * as React from 'react';
import compose from '../../utilities/react-compose';
import { unstyled } from '../shared';
import withRef from '../WithRef';

export interface Props extends React.HTMLProps<HTMLAnchorElement> {
    /** A destination to link to */
    url: string;
    /** Forces url to open in a new tab */
    external?: boolean;
    /**    Content to display inside the link */
    children?: React.ReactNode;

    [key: string]: any;
}

export type LinkLikeComponent = ReactComponent<Props> | undefined;
export type CombinedProps = Props;

export class UnstyledLink extends React.PureComponent<CombinedProps, never> {

    public render() {
        const {external, url, ...rest} = this.props;

        const target = external ? '_blank' : undefined;
        const rel = external ? 'noopener noreferrer' : undefined;
        return (
            <a target={target} {...rest} href={url} rel={rel} {...unstyled.props} />
        );
    }
}

export default compose<Props>(
    withRef<Props>(),
)(UnstyledLink);