import { wrapWithComponent } from '@shopify/react-utilities';
import { classNames } from '@shopify/react-utilities/styles';
import * as React from 'react';

import Scrollable from '../../../Scrollable';

import styles from '../../Popover.scss';
import Section from '../Section';

export interface Props {
    fixed?: boolean;
    sectioned?: boolean;
    children?: React.ReactNode;
}

export default function Pane({fixed, sectioned, children}: Props) {
    const className = classNames(styles.Pane, fixed && styles['Pane-fixed']);

    const content = sectioned ? wrapWithComponent(children, Section) : children;

    return fixed ? (
        <div className={className}>{content}</div>
    ) : (
        <Scrollable hint={true} shadow={true} className={className}>
            {content}
        </Scrollable>
    );
}