import { classNames } from '@shopify/react-utilities';
import * as React from 'react';
import styles from './Backdrop.scss';

export interface Props {
    onClick?(): void;

    onTouchStart?(): void;

    belowNavigation?: boolean;
}

export default function Backdrop(props: Props) {
    const {onClick, onTouchStart, belowNavigation} = props;

    const className = classNames(
        styles.Backdrop,
        belowNavigation && styles.belowNavigation,
    );

    return (
        <React.Fragment>
            <div
                className={className}
                onClick={onClick}
                onTouchStart={onTouchStart}
            />
        </React.Fragment>
    );
}