import { classNames, variationName } from '@shopify/react-utilities';
import * as React from 'react';

import {
    add,
    alert,
    arrowDown,
    arrowLeft,
    arrowRight,
    arrowUp,
    arrowUpDown,
    calendar,
    cancel,
    cancelSmall,
    caretDown,
    caretUp,
    checkmark,
    chevronDown,
    chevronLeft,
    chevronRight,
    chevronUp,
    circleCancel,
    circleChevronDown,
    circleChevronLeft,
    circleChevronRight,
    circleChevronUp,
    circleInformation,
    circlePlus,
    circlePlusOutline,
    conversation,
    del as deleteIcon,
    disable,
    dispute,
    duplicate,
    embed,
    exp as exportIcon,
    external,
    help,
    home,
    horizontalDots,
    imp as importIcon,
    logOut,
    notes,
    notification,
    onlineStore,
    orders,
    print,
    products,
    profile,
    refresh,
    risk,
    save,
    search,
    subtract,
    view,
} from '../../icons';

import styles from './Icon.scss';

export type Color =
    | 'white'
    | 'black'
    | 'skyLighter'
    | 'skyLight'
    | 'sky'
    | 'skyDark'
    | 'inkLightest'
    | 'inkLighter'
    | 'inkLight'
    | 'ink'
    | 'blueLighter'
    | 'blueLight'
    | 'blue'
    | 'blueDark'
    | 'blueDarker'
    | 'indigoLighter'
    | 'indigoLight'
    | 'indigo'
    | 'indigoDark'
    | 'indigoDarker'
    | 'tealLighter'
    | 'tealLight'
    | 'teal'
    | 'tealDark'
    | 'tealDarker'
    | 'greenLighter'
    | 'green'
    | 'greenDark'
    | 'yellowLighter'
    | 'yellow'
    | 'yellowDark'
    | 'orange'
    | 'redLighter'
    | 'red'
    | 'redDark'
    | 'purple';

export const BUNDLED_ICONS = {
    add,
    alert,
    arrowDown,
    arrowLeft,
    arrowRight,
    arrowUp,
    arrowUpDown,
    calendar,
    cancel,
    cancelSmall,
    caretDown,
    caretUp,
    checkmark,
    chevronDown,
    chevronLeft,
    chevronRight,
    chevronUp,
    circleCancel,
    circleChevronDown,
    circleChevronLeft,
    circleChevronRight,
    circleChevronUp,
    circleInformation,
    circlePlus,
    circlePlusOutline,
    conversation,
    delete: deleteIcon,
    disable,
    dispute,
    duplicate,
    embed,
    export: exportIcon,
    external,
    help,
    home,
    horizontalDots,
    import: importIcon,
    logOut,
    notes,
    notification,
    onlineStore,
    orders,
    print,
    products,
    profile,
    refresh,
    risk,
    save,
    search,
    subtract,
    view,
};

export interface SVGSource {
    viewBox: string;
    body: string;
}

export type BundledIcon = keyof typeof BUNDLED_ICONS;

export type IconSource =
    | React.ReactNode
    | 'placeholder'
    | BundledIcon
    | string;

export interface Props {
    /** The SVG contents to display in the icon. Icons should be in a 20 X 20 pixel viewbox */
    source: IconSource;
    /** Sets the color for the SVG fill */
    color?: Color;
    /** Show a backdrop behind the icon */
    backdrop?: boolean;
    /** Descriptive text to be read to screenreaders */
    accessibilityLabel?: string;
}

export type CombinedProps = Props;

function Icon({
                  source,
                  color,
                  backdrop,
                  accessibilityLabel,
              }: CombinedProps) {
    const className = classNames(
        styles.Icon,
        color && styles[variationName('color', color)],
        color && color !== 'white' && styles.isColored,
        backdrop && styles.hasBackdrop,
    );
    let contentMarkup: React.ReactNode;
    if (source === 'placeholder') {
        contentMarkup = <div className={styles.Placeholder}/>;
    } else if (React.isValidElement(source)) {
        contentMarkup = source;
    } else  {

        const iconSource =
            typeof source === 'string' && isBundledIcon(source)
                ? BUNDLED_ICONS[source]
                : source;
        const icon = svgParser(iconSource);
        contentMarkup = icon &&
            icon.viewBox &&
            icon.body && (
                <svg
                    className={styles.Svg}
                    viewBox={icon.viewBox}
                    dangerouslySetInnerHTML={{__html: icon.body}}
                    focusable="false"
                    aria-hidden="true"
                />
            );
    }

    return (
        <span className={className} aria-label={accessibilityLabel}>
      {contentMarkup}
    </span>
    );
}


const VIEWBOX_REGEX = /.*?viewBox=["'](-?[\d\.]+[, ]+-?[\d\.]+[, ][\d\.]+[, ][\d\.]+)["']/;
const SVG_BASE64_REGEX = /data:image\/svg[^,]*?(;base64)?,(.*)/;
const SVG_REGEX = /(<svg[^>]*>|<\/svg>)/g;
const FILL_REGEX = /fill=["'][^"]*?["']/g;
const WHITE_HEX_REGEX = /['"]#fff(?:fff)?['"]/i;

function svgParser(source: string | BundledIcon): SVGSource {

    const decodeSourceMatch = source.match(SVG_BASE64_REGEX);

    if (decodeSourceMatch) {
        const decodeSource =
            decodeSourceMatch[1] ? atob(decodeSourceMatch[2]) : decodeURIComponent(decodeSourceMatch[2]);

        const finalSource = decodeSource.replace(FILL_REGEX, (fill) => {
            return WHITE_HEX_REGEX.test(fill) ? 'fill="currentColor"' : '';
        });
        const matches = finalSource.match(VIEWBOX_REGEX);
        const viewBox = matches && matches.length >= 2 ? matches[1] : '';
        const body = finalSource.replace(SVG_REGEX, '');
        return {viewBox, body};
    } else {
        return {viewBox: '', body: ''};
    }
}


function isBundledIcon(key: string | BundledIcon): key is BundledIcon {
    return Object.keys(BUNDLED_ICONS).includes(key);
}

export default Icon;