import { autobind } from '@shopify/javascript-utilities/decorators';
import isEqual from 'lodash/isEqual';
import * as React from 'react';
import { UserMenuProps } from '../components';
import UserMenuContext, { UserMenuContextTypes } from './context';

interface Props {
    mobileView: boolean;
    children: React.ReactNode;
}

class Provider extends React.Component<Props, UserMenuContextTypes> {
    public static getDerivedStateFromProps(
        {mobileView: nextMobileView}: Props,
        {mobileView}: UserMenuContextTypes,
    ) {
        if (nextMobileView !== mobileView) {
            return {mobileView: nextMobileView};
        }
        return null;
    }

    public state = {
        // eslint-disable-next-line react/no-unused-state
        mobileView: this.props.mobileView,
        mobileUserMenuProps: undefined,
        // eslint-disable-next-line react/no-unused-state
        setMobileUserMenuProps: this.setMobileUserMenuProps,
    };

    public render() {
        const {state} = this;
        const {children} = this.props;
        return (
            <UserMenuContext.Provider value={state}>
                {children}
            </UserMenuContext.Provider>
        );
    }

    @autobind
    private setMobileUserMenuProps(mobileUserMenuProps: UserMenuProps) {
        const {mobileUserMenuProps: prevMobileUserMenuProps} = this.state;
        if (isEqual(mobileUserMenuProps, prevMobileUserMenuProps)) {
            return;
        }
        this.setState({mobileUserMenuProps});
    }
}

export default Provider;