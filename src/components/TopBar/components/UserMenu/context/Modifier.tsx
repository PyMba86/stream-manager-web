import * as React from 'react';
import { WithContextTypes } from '../../../../../types';
import withContext from '../../../../WithContext';
import { UserMenuProps } from '../components';
import Consumer from './Consumer';
import { UserMenuContextTypes } from './context';

interface Props {
    userMenuProps: UserMenuProps;
}

type ComposedProps = Props & WithContextTypes<UserMenuContextTypes>;

class Modifier extends React.Component<ComposedProps, {}> {
    public static getDerivedStateFromProps({
                                        context: {setMobileUserMenuProps},
                                        userMenuProps,
                                    }: ComposedProps) {
        if (setMobileUserMenuProps) {
            setMobileUserMenuProps(userMenuProps);
        }
        return null;
    }

    public state = {};

    public render() {
        return null;
    }
}

export default withContext<Props, {}, UserMenuContextTypes>(Consumer)(Modifier);
