import * as React from 'react';
import { WithContextTypes } from '../../../../types';
import withContext from '../../../WithContext';
import { UserMenu as UserMenuComponent, UserMenuProps } from './components';
import { Consumer as UserMenuConsumer, UserMenuContextTypes } from './context';

type ComposedProps = UserMenuProps & WithContextTypes<UserMenuContextTypes>;

function UserMenu({
                      context: {mobileUserMenuProps, mobileView},
                      ...userMenuProps
                  }: ComposedProps) {
    if (mobileUserMenuProps && mobileView) {
        return <UserMenuComponent {...mobileUserMenuProps} />;
    }
    return <UserMenuComponent {...userMenuProps} />;
}

export default withContext<UserMenuProps, {}, UserMenuContextTypes>(
    UserMenuConsumer,
)(UserMenu);